# Metapackage for Image-based MatuushOS
This is a project that aims to have MatuushOS composing fully of images

You can build it like any other Rust project.

# Dependencies
- `openssl`
- `libarchive`
> Be sure to check [MatuushOS Wiki](http://matuushos.guymatus.tech) for installation instructions.