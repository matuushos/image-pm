#![doc = include_str!("../README.md")]
#![warn(clippy::all, clippy::pedantic)]
#![cfg(target_os = "linux")]

use std::path::Path;

use clap::Parser;

use crate::utils::Cli;

mod utils;

fn main() -> anyhow::Result<(), anyhow::Error> {
    let a: Cli = utils::Cli::parse();
    if !Path::new("/mtos/config").exists() {
        println!(">> Creating one");
        Cli::mkfile()?;
        std::process::exit(255);
    }
    if let Some(install) = a.install {
        for i in install {
            Cli::install(vec![i])?;
        }
    }
    if let Some(remove) = a.remove {
        for i in &remove {
            Cli::remove(i)?;
        }
    }
    if let Some(q) = a.query {
        Cli::query(q)?;
    }
    Ok(())
}