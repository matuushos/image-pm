use std::path::Path;

use ansi_term::Colour::Green;
use clap::Parser;
use fetch_data::download;
use nix::mount::{mount, MsFlags, umount};
use ron::ser::PrettyConfig;
use serde::{Deserialize, Serialize};

#[derive(Default, Parser)]
#[clap(
    name = "pm", 
    about = "Package Manager", 
    long_about = "Package Manager for MatuushOS"
)]
pub(crate) struct Cli {
    #[arg(long)]
    /// Install a package
    pub(crate) install: Option<Vec<String>>,
    ///Remove a package
    #[arg(long)]
    pub(crate) remove: Option<Vec<String>>,
    /// Query a package
    #[arg(long)]
    pub(crate) query: Option<String>,
    /// Basically a refresh repository function
    pub(crate) redownload: Option<String>,
}

#[non_exhaustive]
#[derive(Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub(crate) struct Repos {
    addr: String,
    is_https: String,
    categories: Vec<String>,
    bscriptaddr: String,
}

#[non_exhaustive]
#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct PkgMntPoint {
    folder: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub(crate) struct Cfg {
    repos: Repos,
    pkg_mnt_point: PkgMntPoint,
}

impl Cli {
    pub(crate) fn install(pkgs: Vec<String>) -> anyhow::Result<(), anyhow::Error> {
        let cfg: Cfg = ron::from_str(&std::fs::read_to_string("/mtos/config")?)?;
        for i in pkgs {
            println!(">> Installing {i}");
            download(format!("{}/{i}", cfg.repos.addr), "/mtos/downloaded")?;
            mount(Some(&Path::new("/mtos/downloaded").join(format!("{i}.squashfs"))), &Path::new(&cfg.pkg_mnt_point.folder).join(i), Some("squashfs"), MsFlags::empty(), Some(""))?;
        }
        Ok(())
    }
    pub(crate) fn mkfile() -> anyhow::Result<(), anyhow::Error> {
        let path = "/mtos/config";
        let cfg: Cfg = Cfg {
            repos: Repos {
                addr: String::new(),
                is_https: String::new(),
                categories: vec![],
                bscriptaddr: String::new(),
            },
            pkg_mnt_point: PkgMntPoint {
                folder: String::new()
            },
		};
        std::fs::write(path, ron::ser::to_string_pretty(&cfg,
                                                        PrettyConfig::default())?)?;
        Ok(())
    }
    pub(crate) fn query(pkg: String) -> anyhow::Result<(), anyhow::Error> {
        let f = std::fs::read_to_string("/mtos/config")?;
        let cfg: Cfg = ron::from_str(&f)?;
        let pack = pkg.clone();
        download(format!("{}/{pkg}", cfg.repos.bscriptaddr), "/tmp")?;
        let bcfg: libimager::Builder = ron::from_str(Path::new("/tmp").join(pkg).to_str().unwrap())?;
        let info = format!(">> Information about payload {pack}:").len();
        println!(">> Information about payload {pack}:");
        for i in 0..info {
            print!("-");
            if i == info {
                println!();
            };
        };
        println!("{bcfg:#?}");
        Ok(())
    }
    pub(crate) fn remove(pkgs: &String) -> anyhow::Result<(), anyhow::Error> {
        let g = Green.paint(">>");
        println!("{g} Uninstalling {pkgs}");
        let cfg: Cfg = ron::from_str(&std::fs::read_to_string("/mtos/config")?)?;
        umount(&Path::new(&cfg.pkg_mnt_point.folder).join(pkgs))?;
        println!("{g} Done");
        Ok(())
    }
}