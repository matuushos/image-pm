#![warn(rust_2024_compatibility,
    clippy::all,
    clippy::pedantic,
    clippy::correctness
)]
#![doc = include_str!("../README.md")]

use clap::Parser;

fn main() -> anyhow::Result<(), anyhow::Error> {
    let a = libimager::Cli::parse();
    if let Some(c) = a.build {
        libimager::Builder::construct(c)?;
    } else if let Some(c) = a.create {
        libimager::Builder::mkfile(c)?;
    }
    Ok(())
}