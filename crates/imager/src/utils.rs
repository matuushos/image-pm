use std::{borrow::Borrow, env, fs, path::Path, string::ToString};
use std::env::set_current_dir;
use std::fs::File;

use ansi_term::Colour::{Blue, Green, Red};
use clap::Parser;
use compress_tools::Ownership;
use fetch_data::download;
use git2::Repository;

use ron::ser::PrettyConfig;
use serde::{Deserialize, Serialize};
use squashfs_ng::write::TreeProcessor;
use walkdir::WalkDir;

use crate::OSImageKind::Update;
use crate::TypeOfPayload::{OSImage, Package};

#[derive(Serialize, Deserialize, Debug, Clone)]
#[non_exhaustive]
pub enum OSImageKind {
    #[non_exhaustive]
    Update { version: Vec<i32>, is_major: bool },
    #[non_exhaustive]
    Bootstrap {
        minimal: bool,
        version: Vec<i32>,
        is_major: bool,
    },
}

#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
struct OSPayloadInfo {
    release: Vec<i32>,
    kind: OSImageKind,
    contains_core_pkg_update: bool,
}

/// Type of packages
#[derive(Serialize, Deserialize, Debug, Clone)]
#[non_exhaustive]
pub enum TypeOfPayload {
    ///Operating system image
    OSImage {
        /// Gets the kind of the image
        ///
        kind: OSImageKind,
    },
    ///Package image
    Package {
        /// Checks if the application is graphical. If set to `true`, Imager will symlink the
        /// `.desktop` file to ~/.local/share/applications/PM
        is_graphical: bool,
    },
}

#[derive(Parser)]
#[clap(
    name = "Imager",
    about = "A system for building package/OS update images for MatuushOS",
    long_about = "System that builds images for MatuushOS.\nThe images can be:\n\t- OS update image (though they can be used as bootstrap images)\
    \n\t- Package image\r\n
    These images can be uploaded to package registry (which is then set in `pm`) to allow easy downloads and are sandboxed by Firejail."
)]
/// CLI args for Clap
#[non_exhaustive]
pub struct Cli {
    #[arg(short, long)]
    /// Build the image
    pub build: Option<String>,
    #[arg(short, long)]
    pub create: Option<String>,
}

/// Properties for the metadata file
#[derive(Serialize, Deserialize)]
#[non_exhaustive]
struct MetaFile {
    name: String,
    desc: String,
    type_of_payload: TypeOfPayload,
    sources: Vec<String>,
    paths: Vec<String>,
}

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct Building {
    pub prepare: Vec<String>,
    pub build: Vec<String>,
    pub install: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub struct Builder {
    pub name: String,
    ///Description
    pub description: String,
    ///Type of payload
    pub type_of_payload: TypeOfPayload,
    ///Version
    pub version: Vec<i32>,
    /// Source.
    /// Must be a Git repository
    pub sources: Vec<String>,
    /// Build steps.
    /// Build system performs automatic word splitting to distinct the build command from
    /// arguments
    pub building: Building,
    /// Local dependencies.
    /// Required if used for building an image
    pub localdeps: Vec<String>,
    /// Switch to turn cloning on or off
    is_git: bool,
    /// Filenames.
    /// Can be empty if `is_git` is set to `true`
    filenames: Vec<String>,
}

impl Builder {
    /// Initializes imager configuration file
    ///# How to implement `Vec<String>` into the Command:
    ///1. Take the first element of the array from Vec
    ///2. `let cmd = i.iter();`
    ///3. `Command::new(cmd[0]).args(vec![cmd.next()]);`
    pub fn mkfile(path: String) -> anyhow::Result<(), anyhow::Error> {
        let cfg = Builder {
            name: "".to_string(),
            description: "".to_string(),
            type_of_payload: Package {
                is_graphical: Default::default(),
            },
            version: vec![],
            sources: vec![],
            building: Default::default(),
            localdeps: vec![],
            is_git: false,
            filenames: vec![],
        };
        let p = path;
        fs::write(
            p,
            ron::ser::to_string_pretty(&cfg, PrettyConfig::default())?,
        )?;
        Ok(())
    }
    /// This is used for building the image
    /// # Errors
    /// Will fail if the function can't download the sources, or if prepare/build/install phase fails.
    pub fn construct(path: String) -> anyhow::Result<(), anyhow::Error> {
        let f = fs::read_to_string(path)?;
        let cfg: Self = ron::from_str::<Self>(&f)?;
        let config = cfg.borrow();
        match config.type_of_payload {
            OSImage {
                kind: Update { .. },
            } => Self::build_osimage(
                f,
                &Update {
                    version: vec![],
                    is_major: false,
                },
            ),
            OSImage {
                kind: OSImageKind::Bootstrap { .. },
            } => Self::build_osimage(
                f,
                &OSImageKind::Bootstrap {
                    minimal: false,
                    version: vec![],
                    is_major: false,
                },
            ),
            Package {
                is_graphical: true | false,
            } => {
                // Write metadata file to the package directory
                let meta = MetaFile {
                    name: cfg.name,
                    desc: cfg.description,
                    type_of_payload: cfg.type_of_payload,
                    sources: cfg.sources,
                    paths: vec![],
                };
                for i in &["build", "payload"] {
                    match fs::create_dir(i) {
                        Ok(_) => println!(
                            "{} Successfully created {} directory",
                            Green.paint(">>"),
                            Blue.paint(i.to_string())
                        ),
                        Err(_) => {
                            fs::remove_dir_all(i)?;
                            fs::create_dir_all(i)?;
                            println!("\t{} Created directory {i}", Green.paint(">>"));
                        }
                    };
                }
                println!(
                    "{} Starting build of payload {}, version {:#?}, of which type of payload is {:?}",
                    Green.paint(">>"),
                    Blue.underline().paint(meta.name.clone()),
                    Blue.underline().paint(cfg.version),
                    meta.type_of_payload
                );
                println!("{} Downloading the source", Green.paint(">>"));
                for i in meta.sources {
                    if cfg.is_git {
                        Repository::clone(&i.clone(), Path::new(".").join("build"))?;
                    } else {
                        for t in &cfg.filenames {
                            let tmpdir = env::temp_dir().clone();
                            println!(
                                "{} Downloading {} from {} to {:#?}",
                                Green.paint(">>"),
                                Blue.paint(t),
                                Green.paint(&i),
                                tmpdir.clone()
                            );
                            download(&i, Path::new(&tmpdir).join(t))?;
                            fs::copy(Path::new(&tmpdir).join(t), t)?;
                            compress_tools::uncompress_archive(
                                File::open(t)?,
                                &Path::new(".").join("build"),
                                Ownership::Preserve,
                            )?;
                        }
                    }
                }
                    set_current_dir(Path::new(".").join("build"))?;
                    std::process::Command::new(
                        cfg.building
                            .prepare
                            .clone()
                            .into_iter()
                            .nth(0)
                            .is_some()
                            .to_string(),
                    )
                    .args(&cfg.building.prepare.clone().into_iter().next().clone())
                    .current_dir(".")
                    .output()?;

                    std::process::Command::new(
                        cfg.building
                            .build
                            .clone()
                            .into_iter()
                            .nth(0)
                            .is_some()
                            .to_string(),
                    )
                    .args(&cfg.building.build.clone().into_iter().next().clone())
                    .current_dir(".")
                    .output()?;

                    std::process::Command::new(
                        cfg.building.install.clone().first().is_some().to_string(),
                    )
                    .args(&cfg.building.install.clone().into_iter().next())
                    .current_dir(".")
                    .env("INSTROOT", Path::new("../../payload"))
                    .output()?;
                println!(">> Packaging the built binaries");
                TreeProcessor::new(format!("{}.squashfs", meta.name))?.process("../payload")?;
                Ok(())
            }
        }?;
        Ok(())
    }
    #[doc(hidden)]
    pub(crate) fn construct_localdeps(f: Vec<String>) -> anyhow::Result<(), anyhow::Error> {
        for t in f {
            let cfg: Builder = ron::from_str(&fs::read_to_string(t)?)?;
            let meta = MetaFile {
                name: cfg.name,
                desc: cfg.description,
                type_of_payload: cfg.type_of_payload,
                sources: cfg.sources,
                paths: vec![],
            };
            for i in &["build", "payload"] {
                fs::create_dir(Path::new(&meta.name).join(i))?;
                println!("\t{} Created directory {i}", Green.paint(">>"));
            }
            println!(
                "{} Starting build of payload {}, version {:#?}, of which type of payload is {:#?}",
                Green.paint(">>"),
                meta.name,
                cfg.version,
                meta.type_of_payload
            );
            println!("{} Downloading the source(s)", Green.paint(">>"));
            for i in &meta.sources {
                if cfg.is_git {
                    Repository::clone(i, Path::new(".").join("build"))?;
                } else {
                    for fname in &cfg.filenames {
                        if let Ok(f) = download(i.clone(), format!("build/{fname}")) {
                            println!("{} Successfully downloaded {f:?}", Green.paint(">>"))
                        } else {
                            fs::remove_dir_all(Path::new(&env::temp_dir()).join(fname))?;
                        }
                    }
                    compress_tools::uncompress_archive(
                        File::open(i)?,
                        &Path::new(".").join("build"),
                        Ownership::Preserve,
                    )?;
                }
            }
            std::process::Command::new(
                cfg.building
                    .prepare
                    .clone()
                    .into_iter()
                    .nth(0)
                    .is_some()
                    .to_string(),
            )
            .args(&cfg.building.prepare.into_iter().next())
            .current_dir("build")
            .output()?;

            std::process::Command::new(
                cfg.building
                    .build
                    .clone()
                    .into_iter()
                    .nth(0)
                    .is_some()
                    .to_string(),
            )
            .args(&cfg.building.build.into_iter().next())
            .current_dir("build")
            .output()?;

            std::process::Command::new(
                cfg.building
                    .install
                    .clone()
                    .into_iter()
                    .nth(0)
                    .is_some()
                    .to_string(),
            )
            .args(&cfg.building.install.into_iter().next())
            .current_dir("build")
            .env("INSTROOT", Path::new("../payload"))
            .output()?;
            println!(">> Packaging the built binaries");
            for i in meta.sources.clone() {
                for dir in WalkDir::new("payload").into_iter() {
                    let writefile: MetaFile = MetaFile {
                        name: "".to_string().clone(),
                        desc: "".to_string(),
                        type_of_payload: meta.type_of_payload.clone(),
                        sources: vec![(*i).parse()?],
                        paths: vec![dir?.path().to_path_buf().to_str().unwrap().to_string()],
                    };
                    match fs::write(
                        Path::new("payload").join("info.ron"),
                        ron::ser::to_string_pretty(&writefile, PrettyConfig::default())?,
                    ) {
                        Ok(_) => (),
                        Err(_) => continue,
                    }
                }
            }
            set_current_dir("..")?;
            TreeProcessor::new(format!("{}.squashfs", meta.name))?.process("payload")?;
        }
        Ok(())
        // Write metadata file to the package directory
    }
    #[doc(hidden)]
    fn build_osimage(file: String, kind: &OSImageKind) -> anyhow::Result<(), anyhow::Error> {
        let cfg: Builder = ron::from_str::<Builder>(&fs::read_to_string(file)?)?;
        let arrow = Green.paint(">>");
        let exmark = Red.paint("!!");
        match kind {
            Update { .. } => {
                println!(
                    "{} Making an update image\n{} Creating directories",
                    arrow, arrow
                );
                for i in &["build/", "build/mounted", "build/etc", "build/firejail"] {
                    fs::create_dir_all(i)?;
                    println!("\t{} Created directory {i}", arrow);
                }
                let meta: OSPayloadInfo = OSPayloadInfo {
                    release: (*cfg.version).to_owned(),
                    kind: Update {
                        version: cfg.version,
                        is_major: false,
                    },
                    contains_core_pkg_update: false,
                };
                match fs::write(
                    "build/updinfo.ron",
                    ron::ser::to_string_pretty(&meta, PrettyConfig::default())?,
                ) {
                    Ok(f) => println!("{} Metadata file {f:#?} sucessfully written", arrow),
                    Err(e) => {
                        eprintln!("{} An error happened.\n!! Reason: {e:#?}", exmark);
                    }
                }
                fs::create_dir("packages")?;
                for i in cfg.localdeps {
                    Self::construct_localdeps(vec![i])?;
                }
                // Assuming that build scripts installed the sources in root of the payload, we won't have to compress the packages folder
                println!(">> Built the update payload\n>> Compressing the payload");
                std::process::Command::new("mksquashfs")
                    .arg("build")
                    .output()?;
                println!(">> DONE");
            }
            OSImageKind::Bootstrap { .. } => unimplemented!("Not yet implemented, unfortunately"),
        };
        Ok(())
    }
}
